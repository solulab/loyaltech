//
//  ViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 23/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class LoginiVewController: UIViewController {

    //MARK:
    //MARK: Declaration

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    // Contrain Layout
    @IBOutlet weak var LayoutTopForgotPassword: NSLayoutConstraint!
    @IBOutlet weak var LayoutTopSignIn: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if(IS_IPHONE_5 || IS_IPHONE_4){
            LayoutTopForgotPassword.constant = 20
            LayoutTopSignIn.constant = 20
        }

    }

    //MARK:
    //MARK: Button Events

    @IBAction func btnForgotPassword(_ sender: UIButton) {
    pushToController(viewController: "ForgotPasswordViewController")
    }
    @IBAction func btnSignIn(_ sender: UIButton) {
        let emailString = txtEmail.text?.trimmingCharacters(in: .whitespaces)
        let passwodString = txtPassword.text?.trimmingCharacters(in: .whitespaces)
        
        if(emailString == ""){
            displayAlert(title: zAppName, message: zEmailValidation)
        }else if(passwodString == ""){
            displayAlert(title: zAppName, message: zPasswordValidation)
        }else if((passwodString!.characters.count) < 6){
            displayAlert(title: zAppName, message: zPasswordCharacter)
        }else if(isValidEmail(testStr: txtEmail.text!)){
            displayState(zAppName, message: "How will you use Loyal Techs?")
        }else{
            displayAlert(title: zAppName, message: zEmailFailValidate)
        }
    }
    @IBAction func btnFacebook(_ sender: UIButton) {
  
    }
    @IBAction func btnGoogle(_ sender: UIButton) {
  
    }
    @IBAction func btnSignUpAccount(_ sender: UIButton) {
        pushToController(viewController: "SignUpViewController")
    }
    

    //MARK: AlertView
    func displayAlert(title : String, message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil));
        self.present(alert, animated: true, completion: nil)
    }
    
    //Display State
    func displayState(_ title: String, message: String){
        
        let stateAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        stateAlert.addAction(UIAlertAction(title: "Tech", style: .default, handler: { (action: UIAlertAction!) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let clientVC = storyboard.instantiateViewController(withIdentifier: "LandingTechViewController") as! LandingTechViewController
            loggedInAs = "Tech"
            self.navigationController?.pushViewController(clientVC, animated: true)
        }))
        
        stateAlert.addAction(UIAlertAction(title: "Client", style: .cancel, handler: { (action: UIAlertAction!) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let clientVC = storyboard.instantiateViewController(withIdentifier: "LandingClientViewController") as! LandingClientViewController
            loggedInAs = "Client"
            self.navigationController?.pushViewController(clientVC, animated: true)
        }))        
        present(stateAlert, animated: true, completion: nil)
    }

    
    //MARK:
    //MARK: Warning

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

