//
//  Declaration.swift
//  LoyalTECHS
//
//  Created by Rajat on 23/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import Foundation

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//                                  Declaration                             //
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

let zAppName = "LoyalTechs"
let zEmailValidation = "Please enter email address."
let zPasswordValidation = "Please enter password."
let zPasswordCharacter = "Password length should be atleast 6 character."
let zEmailFailValidate = "Please enter valid email address."
let zLogoutMessage = "Do you want to logout?"
let zYES = "Yes"
let zNO = "No"
let zOK = "Ok"


let appDelegate         = UIApplication.shared.delegate as! AppDelegate
let IS_IPHONE_5         = UIScreen.main.bounds.size.height == 568 ? true : false as Bool
let IS_IPHONE_4         = UIScreen.main.bounds.size.height == 480 ? true : false as Bool
let IS_IPHONE_6         = UIScreen.main.bounds.size.height == 667 ? true : false as Bool
let IS_IPHONE_6_plus    = UIScreen.main.bounds.size.height == 736 ? true : false as Bool
let IS_IOS8_AND_UP      = (UIDevice.current.systemVersion as NSString).floatValue >= 8.0
let IS_iPad             = UIScreen.main.bounds.size.height == 1024 ? true : false as Bool
let device_id           = UIDevice.current.identifierForVendor!.uuidString
public var storyBoard   = UIStoryboard(name: "Main", bundle: nil)
let Application         = UIApplication.shared
let Default             = UserDefaults.standard
let NotificationCallTechnician = "CallTechnician"

var loggedInAs = ""

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//                                    Methods                               //
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

extension UIViewController {
    
    func pushToController(viewController: String) {
        self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: viewController))!, animated: true)
    }
    
    func presentTo(viewController : String){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: viewController)
        self.present(vc!, animated: true, completion: nil)
    }
    
    func popView(){
        self.navigationController!.popViewController(animated: true)
    }
    
    func PopoutTologout(){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is LoginiVewController {
                _ = self.navigationController?.popToViewController(vc as! LoginiVewController, animated: true)
            }
        }
    }
}


//MARK: Validation
func isValidEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

func viewCornerRadius(view : UIView, border : Bool){
    view.clipsToBounds = true
    view.layer.cornerRadius = 7
    if(border){
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.init(red: 77.0/255.0, green: 160.0/255.0, blue: 247.0/255.0, alpha: 1).cgColor
    }
}



//MARK:
//MARK: Animation

func btnAnimation(btn: UIButton){
    UIView.animate(withDuration: 0.5, delay: 0.3, options: [.repeat, .transitionFlipFromRight, .autoreverse], animations: {
        UIView.setAnimationRepeatCount(3)
        btn.frame.origin.x = btn.frame.origin.x + 10
    }, completion: { finished in
        btn.frame.origin.x = btn.frame.origin.x - 10
        btn.isHidden = true
    })
}

//MARK: Image Generator

func imageGenerator(name : UILabel, imgView : UIImageView){
    let lblNameInitialize = UILabel()
    lblNameInitialize.frame.size = CGSize(width: 100.0, height: 100.0)
    lblNameInitialize.textColor = UIColor.white
    lblNameInitialize.font = UIFont.systemFont(ofSize: 50.0)
    lblNameInitialize.text = String(name.text!.characters.first!)
    lblNameInitialize.textAlignment = NSTextAlignment.center
    lblNameInitialize.backgroundColor = UIColor.init(red: 72.0/255.0, green: 72.0/255.0, blue: 72.0/255.0, alpha: 1.0)
    lblNameInitialize.clipsToBounds = true
    lblNameInitialize.layer.cornerRadius = 50.0
    
    UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
    lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
    imgView.image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
}


class BaseViewController : UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.5
        blurEffectView.frame = self.view.bounds
        self.view.insertSubview(blurEffectView, at: 1)
    }
}



