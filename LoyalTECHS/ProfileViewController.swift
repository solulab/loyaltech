//
//  ProfileViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 23/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,sideDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: SideMenu Delegate Method
    func pushToViewController(_ controller: UIViewController) {
        //        swrevalVC.revealToggle(btnMenu)
        self.navigationController?.pushViewController(controller, animated: false)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
