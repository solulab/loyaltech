//
//  LandingViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 23/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit
import MapKit

class LandingClientViewController: UIViewController,sideDelegate {

    //MARK:
    //MARK: Declaration

    @IBOutlet weak var mapVIew: MKMapView!
    @IBOutlet weak var btnCallaTech: UIButton!
    
    var swrevalVC : SWRevealViewController!
    
    //PopUp
    @IBOutlet var callTechView: UIView!
    @IBOutlet weak var imgProfileView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPlatform: UILabel!
    @IBOutlet weak var lblServiceLocation: UILabel!

    @IBOutlet weak var btnMunu: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var btnTechnicianEnRoute: UIButton!
    
    
    //Constrain Layout
    
    @IBOutlet weak var LayoutLeftDistance: NSLayoutConstraint!
    @IBOutlet weak var LayoutLeftName: NSLayoutConstraint!
    @IBOutlet weak var LayoutLeftETA: NSLayoutConstraint!
    @IBOutlet weak var LayoutLeftProfilePicture: NSLayoutConstraint!
    
    //MARK:
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        swrevalVC = self.revealViewController()
        swrevalVC.panGestureRecognizer()
        swrevalVC.tapGestureRecognizer()
        appDelegate.sideMenu.delegatePush = self
        self.revealViewController().panGestureRecognizer().isEnabled = true

        DispatchQueue.main.async {
            self.callTechView.frame = CGRect(x: 8, y: self.view.frame.height-308, width: self.view.frame.size.width - 16, height: 300)
            self.view.insertSubview(self.callTechView, at: 5)
            self.callTechView.isHidden = true
            self.btnTechnicianEnRoute.isHidden = true
            viewCornerRadius(view: self.callTechView, border: false)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(LandingClientViewController.callTechnician), name: NSNotification.Name(rawValue: NotificationCallTechnician), object: nil)
        
        imageGenerator(name: lblName, imgView: imgProfileView)
        if(IS_IPHONE_4 || IS_IPHONE_5){
            LayoutLeftName.constant = 5
            LayoutLeftDistance.constant = 5
            LayoutLeftETA.constant =
            5
            LayoutLeftProfilePicture.constant = 8
        }
    }
    
    func callTechnician(){
        self.callTechView.isHidden = false
        btnTechnicianEnRoute.isHidden = false
        btnAnimation(btn: btnTechnicianEnRoute)
    }
    

    //MARK:
    //MARK: Button Events

    @IBAction func btnCallaTech(_ sender: UIButton) {
        pushToController(viewController: "ClientDescriptionViewController")
    }
    @IBAction func btnCancel(_ sender: UIButton) {
        self.callTechView.isHidden = true
        btnTechnicianEnRoute.isHidden = true
    }
    @IBAction func btnMenu(_ sender: UIButton) {
        swrevalVC.revealToggle(sender)
    }
    @IBAction func btnLocation(_ sender: UIButton) {
    
    }
    @IBAction func btnUser(_ sender: UIButton) {
        pushToController(viewController: "ClientDescriptionViewController")
    }
    
    
    //MARK:
    //MARK: Warning
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Delegate Method
    
    func pushToViewController(_ controller: UIViewController) {
        swrevalVC.revealToggle(nil)
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
}
