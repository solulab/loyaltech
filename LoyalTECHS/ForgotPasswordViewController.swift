//
//  ForgotPasswordViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 23/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    //MARK:
    //MARK: Declaration

    @IBOutlet weak var btnEmail: UITextField!
    @IBOutlet var successView: UIView!

    @IBOutlet weak var lblMailId: UILabel!
    //MARK:
    //MARK: ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        DispatchQueue.main.async {
            self.successView.frame = self.view.bounds
            self.view.insertSubview(self.successView, at: 4)
            self.successView.isHidden = true;
        }

    }
    
    //MARK:
    //MARK: Button Events

    @IBAction func btnBack(_ sender: UIButton) {
        popView()
    }
    @IBAction func btnSend(_ sender: UIButton) {
        
        let emailString = btnEmail.text?.trimmingCharacters(in: .whitespaces)
        if(emailString == ""){
            displayAlert(title: zAppName, message: zEmailValidation)
        }else if(isValidEmail(testStr: btnEmail.text!)){
            lblMailId.text = emailString
            self.successView.isHidden = false
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(ForgotPasswordViewController.dismissView), userInfo: nil, repeats: false)
        }else{
            displayAlert(title: zAppName, message: zEmailFailValidate)

        }
    }
    
    
    func dismissView(){
        popView()
        successView.isHidden = true
    }
    
    //MARK: AlertView
    func displayAlert(title : String, message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil));
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:
    //MARK: Warning

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
