//
//  LandingTechViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 24/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit
import MapKit

class LandingTechViewController: UIViewController {

    //MARK:
    //MARK: Declaration
    
    @IBOutlet weak var mapVIew: MKMapView!
    
    //popoup
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPlatform: UILabel!
    @IBOutlet weak var lblServiceLocation: UILabel!
    
    
    //MARK:
    //MARK: VeiwDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    //MARK:
    //MARK: Button Events

    
    @IBAction func btnMainCancelOffer(_ sender: UIButton) {
  
    }
    @IBAction func btnCancelCall(_ sender: UIButton) {
   
    }
    @IBAction func btnStartService(_ sender: UIButton) {
    
    }
    
    //MARK:
    //MARK: Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
