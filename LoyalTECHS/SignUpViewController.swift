//
//  SignUpViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 23/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    //MARK:
    //MARK: Declaration

    @IBOutlet weak var btnImclient: UIButton!
    @IBOutlet weak var btnImtech: UIButton!
    @IBOutlet weak var imgClientIndicator: UIImageView!
    @IBOutlet weak var imgTechIndicator: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRepeatPassword: UITextField!
    
    
    
    
    
    //MARK:
    //MARK: ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    //MARK:
    //MARK: Button Events

    @IBAction func btnImClient(_ sender: UIButton) {
   
    }
    
    @IBAction func btnImTech(_ sender: UIButton) {
    
    }
    
    @IBAction func btnSignUp(_ sender: UIButton) {
  
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        popView()
    }
    
    //MARK:
    //MARK: Warning

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
