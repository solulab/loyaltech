//
//  TechDescriptionViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 24/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class TechDescriptionViewController: UIViewController {

    
    //MARK:
    //MARK: Declaration
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    @IBOutlet weak var lblDeviceType: UILabel!
    @IBOutlet weak var lblPlatform: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    
    
    //MARK:
    //MARK: DidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    //MARK:
    //MARK: Button Events
    
    @IBAction func btnAcceptOffer(_ sender: UIButton) {
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
 
    }
    
    
    //MARK:
    //MARK: Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
