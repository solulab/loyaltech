//
//  ContinueServiceViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 24/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class ContinueServiceViewController: UIViewController {

    
    //MARK:
    //MARK: Declaration

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblObj: UITableView!
    
    
    //MARK:
    //MARK: ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //MARK:
    //MARK: Button Events
    
    @IBAction func btnDone(_ sender: UIButton) {
    
    }
    @IBAction func btnCancel(_ sender: UIButton) {
    
    }
    
    //MARK:
    //MARK: Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class continueServiceCellClass : UITableViewCell{
    
}
