//
//  HelpViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 24/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit
import MapKit

class HelpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    //MARK: Declaration

    @IBOutlet weak var tblObj: UITableView!
    @IBOutlet var cellHeader: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTimeStamp: UILabel!
    @IBOutlet weak var mapView: MKMapView!
   
    
    
    let helpCellString = "helpCell"

    //MARK:
    //MARK: DidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK:
    //MARK: TableView Delegate Methods
    
    // number of rows in table view
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(section == 0){
            return 1
        }
        return 6
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return cellHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: helpCellString) as! helpCellClass!
        if cell == nil {
            cell = helpCellClass(style: UITableViewCellStyle.default, reuseIdentifier:
                helpCellString)
        }
        if(indexPath.section == 0){
            cell?.lblTitle!.text = "Report an issue with your service"
        }else{
            cell?.lblTitle!.text = "Topic \(indexPath.row + 1)"
        }
        cell?.lblTitle!.textColor = UIColor.white
        return cell!
        
    }
    
    //MARK:
    //MARK: Button Events

    @IBAction func btnCancel(_ sender: UIButton) {
        popView()
    }
    
    //MARK:
    //MARK: Warninig
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class helpCellClass : UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    
}
