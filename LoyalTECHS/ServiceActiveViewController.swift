//
//  ServiceActiveViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 24/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class ServiceActiveViewController: UIViewController {

    //MARK:
    //MARK: Declaration
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var tblObj: UITableView!
    
    
    
    //MARK:
    //MARK: ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    //MARK:
    //MARK: Button Events
    
    @IBAction func btnMenu(_ sender: UIButton) {
    
    }
    @IBAction func btnStop(_ sender: UIButton) {
    
    }
    @IBAction func btnContinue(_ sender: UIButton) {
    
    }
    
    
    
    //MARK:
    //MARK: Warning

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

class serviceCellClass : UITableViewCell{
    
}
