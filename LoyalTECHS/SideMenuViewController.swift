//
//  SideMenuViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 23/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

protocol sideDelegate
{
    func pushToViewController(_ controller : UIViewController)
}


class SideMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK:
    //MARK: Declaration

    @IBOutlet weak var tblObj: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    let cellIdentifier = "sideCell"
    var menuArray : [String] = []
    var imgArray : [String] = []

    var delegatePush : sideDelegate!

    //MARK:
    //MARK: ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        if(loggedInAs == "Client"){
            menuArray = ["Profile", "Payment", "Help","","Logout"]
            imgArray = ["profileIcon", "paymentIcon", "helpIcon","","logoutIcon"]

        }else{
            menuArray = ["Profile", "Toolbox", "Qualification","Help","","Logout"]
            imgArray = ["profileIcon", "toolboxIcon", "qualifyIcon","helpIcon","","logoutIcon"]
        }
        imageGenerator(name: lblName, imgView: imgProfile)
        tblObj.tableFooterView = UIView()
        self.tblObj.separatorColor = UIColor.darkGray
    }
    
    //MARK:
    //MARK: Button Events
    
    @IBAction func btnLogout(_ sender: UIButton) {
   
    }
    

    
    //MARK:
    //MARK: TableView Delegate Methods
    
    // number of rows in table view
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return menuArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! sideMenuCell!
        if cell == nil {
            cell = sideMenuCell(style: UITableViewCellStyle.default, reuseIdentifier:
                cellIdentifier)
        }
        cell?.lblTitle.text = menuArray[indexPath.row]
        cell?.imgTitle.image = UIImage(named: imgArray[indexPath.row])
        cell!.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblObj.deselectRow(at: indexPath, animated: true)
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let ProfileVC : ProfileViewController!
        let HelpVC : HelpViewController!
        
        if(indexPath.row == 0){
            ProfileVC = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.delegatePush.pushToViewController(ProfileVC)
        }else if(indexPath.row == 2){
            HelpVC = storyBoard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            self.delegatePush.pushToViewController(HelpVC)
        }else if(indexPath.row == 4){
            self.displayAlert(zAppName, message: zLogoutMessage)
            
//            PopoutTologout()
        }
    }
    
    
    //MARK: Display Alert
    func displayAlert(_ title: String, message: String){
        
        let stateAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        stateAlert.addAction(UIAlertAction(title: zYES, style: .default, handler: { (action: UIAlertAction!) in

        }))
        
        stateAlert.addAction(UIAlertAction(title: zNO, style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        present(stateAlert, animated: true, completion: nil)
    }
    
    //MARK:
    //MARK: Warning

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class sideMenuCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
}
