//
//  ClientDescriptionViewController.swift
//  LoyalTECHS
//
//  Created by Rajat on 24/01/17.
//  Copyright © 2017 solulabInc. All rights reserved.
//

import UIKit

class ClientDescriptionViewController: UIViewController {

    
    //MARK:
    //MARK: Declaration

    @IBOutlet weak var txtProblemDesc: UITextView!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnTab: UIButton!
    @IBOutlet weak var btnDeskTop: UIButton!
    @IBOutlet weak var btnPlatform: UIButton!
    @IBOutlet weak var imgArrow: UIImageView!
    
    //LayoutConstrin
    
    @IBOutlet weak var LayoutBottomCategory: NSLayoutConstraint!
    @IBOutlet weak var LayoutBottomPlatformText: NSLayoutConstraint!

    
    //MARK:
    //MARK: ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(IS_IPHONE_5 || IS_IPHONE_4){
            LayoutBottomPlatformText.constant = 20
            LayoutBottomCategory.constant = 23
        }
    }

    
    //MARK:
    //MARK: Button Events
    
    @IBAction func btnCancel(_ sender: UIButton) {
        popView()
    }
    @IBAction func btnPhone(_ sender: UIButton) {
    
    }
    @IBAction func btnTab(_ sender: UIButton) {
    
    }
    @IBAction func btnDesktop(_ sender: UIButton) {
  
    }
    @IBAction func btnPlatform(_ sender: UIButton) {
  
    }
    @IBAction func btnCall(_ sender: UIButton) {
        popView()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationCallTechnician), object: nil)
    }
    
    //MARK:
    //MARK: Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
